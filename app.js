//required
const argv = require('./config/yargs').argv;
const colors = require('colors');

const { crearTabla, listarTabla } = require('./multiplicar/multiplicar')

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite)
            .then(resp => console.log(`${ resp }`))
            .catch(e => {
                console.log(e);
            })
        break;
    case 'crear':
        crearTabla(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado: ${ archivo }`))
            .catch(e => {
                console.log(e);
            })
        break;
    default:
        console.log('Comando no reconocido');
}
/* for (i = 2; i <= 10; i += 2) {
    console.log(i);
} */
//console.log(argv);
/* const fs = require('fs')

let base = 2;

let data = '';

for (let i = 1; i <= 10; i++) {
    data += (`${base} * ${i} = ${ base * i }\n`);
}

fs.writeFile(`archivos/tabla-${base}.txt`, data, (err) => {
    if (err) throw err;
    console.log(`El archivo tabla-${base}.txt ha sido creado!`);
}); */