const fs = require('fs')
const colors = require('colors');

let listarTabla = async(base, limite) => {
    if (!Number(base)) {
        throw (`El valor introducido ${base} no es un número`);
    }
    let data = '';
    console.log('============================'.green);
    console.log(`tabla de ${base}`.green);
    console.log('============================'.green);
    for (let i = 1; i <= limite; i++) {
        data += `${base} * ${i} = ${base*i}\n`;
    }
    return data;
}

let crearTabla = async(base, limite) => {
    if (!Number(base)) {
        throw (`El valor introducido ${base} no es un número`);
    }
    let data = '';
    for (let i = 1; i <= limite; i++) {
        data += `${base} * ${i} = ${base*i}\n`;
    }
    fs.writeFile(`archivos/tabla-${base}-al-${limite}.txt`, data, (err) => {
        if (err) throw err;
    });
    return (`tabla-${base}-al-${limite}`).green
}

module.exports = {
    crearTabla,
    listarTabla
}